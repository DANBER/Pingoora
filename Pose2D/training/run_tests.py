import matplotlib.pyplot as plt
import numpy as np
import tqdm
from scode import utils
from scode.pipelines import pl_top_down

# ==================================================================================================


def show_images(sample):

    fig = plt.figure(figsize=(25, 5))

    fig.add_subplot(1, 3, 1)
    plt.imshow(sample["image"][0])
    fig.add_subplot(1, 3, 2)
    plt.imshow(sample["heatmap_combined"][0])
    fig.add_subplot(1, 3, 3)
    plt.imshow(np.expand_dims(sample["heatmap"][0, :, :, 0], axis=-1))

    plt.show()


# ==================================================================================================


def test_pipeline_single_sample(sample_data_path):
    config = utils.get_config()

    # Print the first sample of the top-down dataset
    tds = pl_top_down.create_pipeline(sample_data_path, 1, config, mode="train")
    for samples in tds:
        print(samples)
        show_images(samples)
        break

# ==================================================================================================


def test_full_pipeline(sample_data_path):
    bench_batch_size = 16
    config = utils.get_config()

    # Run pipeline for one epoch to check how long preprocessing takes
    print("\nGoing through dataset to check preprocessing duration...")
    tds = pl_top_down.create_pipeline(
        sample_data_path, bench_batch_size, config, mode="train"
    )
    for _ in tqdm.tqdm(tds):
        pass


# ==================================================================================================


def print_config():
    config = utils.get_config()
    print(config)


# ==================================================================================================

if __name__ == "__main__":
    print("\n======================================================================\n")

    # print_config()
    test_pipeline_single_sample("/datasets/coco2017/annotations/person_keypoints_val2017.json")
    # test_full_pipeline("/datasets/coco2017/annotations/person_keypoints_val2017.json")
    print("FINISHED")
