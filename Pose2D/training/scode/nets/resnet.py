import tensorflow as tf
from tensorflow.keras import layers as tfl

from .. import utils

# ==================================================================================================


class MyModel(tf.keras.Model):  # pylint: disable=abstract-method
    def __init__(self, netconfig: dict, image_params: dict):
        """
        Implementation of "Simple baselines for human pose estimation and tracking"

        Sample netconfig:
          name: "resnet"
          l2_loss: 0.00001
          freeze_basenet: false
        """
        super().__init__()

        self.num_joints = image_params["num_joints"]
        self.input_height = image_params["input_height"]
        self.input_width = image_params["input_width"]

        self.freeze_basenet = netconfig["freeze_basenet"]
        model = self.make_model()

        if "l2_loss" in netconfig:
            self.l2_loss = netconfig["l2_loss"]
            model = utils.add_regularization(
                model, tf.keras.regularizers.l2(self.l2_loss)
            )

        self.model = model

    # ==============================================================================================

    def make_model(self):
        input_tensor = tfl.Input(
            shape=[self.input_height, self.input_width, 3], name="input"
        )

        # Used for easier debugging changes
        x = tf.identity(input_tensor)

        # Use a pretrained model as basis
        base_model = tf.keras.applications.resnet50.ResNet50(
            include_top=False,
            weights="imagenet",
            input_tensor=x,
            input_shape=None,
            pooling=None,
            classifier_activation="softmax",
        )
        x = base_model.output

        # Optionally freeze layers of base model
        if self.freeze_basenet:
            for layer in base_model.layers:
                layer.trainable = False

        # Deconvolution layers
        # https://arxiv.org/pdf/1804.06208.pdf (chapter 2, paragraph 2)
        for _ in range(3):
            x = tfl.Conv2DTranspose(
                filters=256, kernel_size=4, strides=2, padding="same", use_bias=False
            )(x)
            x = tfl.BatchNormalization(momentum=0.9)(x)
            x = tfl.ReLU()(x)

        # One heatmap for each joint
        x = tfl.Conv2D(filters=self.num_joints, kernel_size=1, use_bias=True)(x)

        # Cast last layer to float32 for numeric stability with mixed precision
        output_tensor = tf.cast(x, dtype=tf.float32, name="output")

        model = tf.keras.Model(input_tensor, output_tensor, name="ResNet50")
        return model

    # ==============================================================================================

    def summary(self, line_length=100, **kwargs):  # pylint: disable=arguments-differ
        print("")
        self.model.summary(line_length=line_length, **kwargs)

    # ==============================================================================================

    @tf.function()
    def call(self, x, training=False):  # pylint: disable=arguments-differ
        """Call with RGB image"""

        x = self.model(x, training=training)
        return x
