import numpy as np
import tensorflow as tf
from tensorflow.keras import layers as tfl

# ==================================================================================================


class Bottleneck(tfl.Layer):  # pylint: disable=abstract-method
    def __init__(self, filters):
        super().__init__()
        self.expansion = 4

        self.conv1 = tfl.Conv2D(
            filters=filters,
            kernel_size=1,
            padding="same",
            use_bias=False,
        )
        self.bnorm1 = tfl.BatchNormalization(momentum=0.9)

        self.conv2 = tfl.Conv2D(
            filters=filters, kernel_size=3, padding="same", use_bias=False
        )
        self.bnorm2 = tfl.BatchNormalization(momentum=0.9)

        self.conv3 = tfl.Conv2D(
            filters=filters * self.expansion,
            kernel_size=1,
            padding="same",
            use_bias=False,
        )
        self.bnorm3 = tfl.BatchNormalization(momentum=0.9)

        self.conv_res = tfl.Conv2D(
            filters=filters * self.expansion,
            kernel_size=1,
            padding="same",
            use_bias=False,
        )
        self.bnorm_res = tfl.BatchNormalization(momentum=0.9)

    # ==============================================================================================

    def call(self, x):  # pylint: disable=arguments-differ
        residual = x

        x = self.conv1(x)
        x = self.bnorm1(x)
        x = tf.nn.relu(x)

        x = self.conv2(x)
        x = self.bnorm2(x)
        x = tf.nn.relu(x)

        x = self.conv3(x)
        x = self.bnorm3(x)

        if x.shape[-1] != residual.shape[-1]:
            residual = self.conv_res(residual)
            residual = self.bnorm_res(residual)

        x = x + residual
        x = tf.nn.relu(x)

        return x


# ==================================================================================================


class BasicBlock(tfl.Layer):  # pylint: disable=abstract-method
    def __init__(self, filters, stride=1):
        super().__init__()
        self.stride = stride

        self.conv1 = tfl.Conv2D(
            filters=filters,
            kernel_size=3,
            strides=self.stride,
            padding="same",
            use_bias=False,
        )
        self.bnorm1 = tfl.BatchNormalization(momentum=0.9)

        self.conv2 = tfl.Conv2D(
            filters=filters, kernel_size=3, padding="same", use_bias=False
        )
        self.bnorm2 = tfl.BatchNormalization(momentum=0.9)

        self.conv_res = tfl.Conv2D(
            filters=filters,
            kernel_size=1,
            strides=self.stride,
            padding="same",
            use_bias=False,
        )
        self.bnorm_res = tfl.BatchNormalization(momentum=0.9)

    # ==============================================================================================

    def call(self, x):  # pylint: disable=arguments-differ
        residual = x

        x = self.conv1(x)
        x = self.bnorm1(x)
        x = tf.nn.relu(x)

        x = self.conv2(x)
        x = self.bnorm2(x)

        if self.stride != 1 or x.shape[-1] != residual.shape[-1]:
            residual = self.conv_res(residual)
            residual = self.bnorm_res(residual)

        x = x + residual
        x = tf.nn.relu(x)

        return x


# ==================================================================================================


class DownsampleLayer(tfl.Layer):  # pylint: disable=abstract-method
    def __init__(self, filters):
        super().__init__()

        self.conv = tfl.Conv2D(
            filters=filters,
            kernel_size=3,
            strides=2,
            padding="same",
            use_bias=False,
        )
        self.bnorm = tfl.BatchNormalization(momentum=0.9)

    # ==============================================================================================

    def call(self, x):  # pylint: disable=arguments-differ

        x = self.conv(x)
        x = self.bnorm(x)
        x = tf.nn.relu(x)
        return x


# ==================================================================================================


class UpsampleLayer(tfl.Layer):  # pylint: disable=abstract-method
    def __init__(self, filters, scale):
        super().__init__()

        self.conv = tfl.Conv2D(
            filters=filters,
            kernel_size=1,
            use_bias=False,
        )
        self.bnorm = tfl.BatchNormalization(momentum=0.9)
        self.up = tfl.UpSampling2D(size=scale, interpolation="nearest")

    # ==============================================================================================

    def call(self, x):  # pylint: disable=arguments-differ

        x = self.conv(x)
        x = self.bnorm(x)
        x = self.up(x)
        return x


# ==================================================================================================


def split_new_branch(input_branches, branch_channels):
    """Downsample the last branch to create a new branch"""

    branch_count = len(input_branches)
    x = input_branches[-1]
    branch = DownsampleLayer(filters=branch_channels[branch_count])(x)

    return branch


# ==================================================================================================


def add_exchange_unit(input_branches, branch_channels):
    """Exchange branch information through up or downsampling features and adding them together"""

    new_branches = []
    for i in range(len(input_branches)):

        branch_inputs = []
        for j in range(len(input_branches)):
            x = input_branches[j]

            if j == i:
                branch_inputs.append(x)

            elif j > i:
                x = UpsampleLayer(filters=branch_channels[i], scale=2 ** (j - i))(x)
                branch_inputs.append(x)

            else:
                # According to the reference implementations the channel width is kept until the
                # last downsampling layer which matches it to the current branch
                for _ in range(j, i - 1):
                    x = DownsampleLayer(filters=branch_channels[j])(x)
                x = DownsampleLayer(filters=branch_channels[i])(x)
                branch_inputs.append(x)

        branch = tf.add_n(branch_inputs)
        branch = tf.nn.relu(branch)
        new_branches.append(branch)
    return new_branches


# ==================================================================================================


def add_residual_units(input_branches, branch_channels, units=4):
    """To each branch add some residual blocks"""

    output_branches = []
    for i in range(len(input_branches)):
        x = input_branches[i]

        for _ in range(units):
            x = BasicBlock(filters=branch_channels[i])(x)
        output_branches.append(x)

    return output_branches


# ==================================================================================================


class MyModel(tf.keras.Model):  # pylint: disable=abstract-method
    def __init__(self, netconfig: dict, image_params: dict):
        """
        Implementation of "Deep High-Resolution Representation Learning for Human Pose Estimation"

        Sample netconfig:
          name: "hrnet"
          W: 32
          stage_blocks: [4, 1, 4, 3]
          l2_loss: 0.00001
        """
        super().__init__()

        self.num_joints = image_params["num_joints"]
        self.input_height = image_params["input_height"]
        self.input_width = image_params["input_width"]
        self.stage_blocks = netconfig["stage_blocks"]

        scrange = [2 ** i for i in range(len(self.stage_blocks))]
        self.stage_channels = (np.array(scrange) * int(netconfig["W"])).tolist()

        self.model = self.make_model()

    # ==============================================================================================

    def make_model(self):
        input_tensor = tfl.Input(
            shape=[self.input_height, self.input_width, 3], name="input"
        )

        # Used for easier debugging changes
        x = tf.identity(input_tensor)

        # Stem: Reducing input size to output size
        for _ in range(2):
            x = tfl.Conv2D(
                filters=64, kernel_size=3, strides=2, padding="same", use_bias=False
            )(x)
            x = tfl.BatchNormalization(momentum=0.9)(x)
            x = tf.nn.relu(x)

        # Stage 1
        for _ in range(self.stage_blocks[0]):
            x = Bottleneck(filters=64)(x)

        # Transition 1-2
        nbranch = split_new_branch([x], self.stage_channels)
        x = tfl.Conv2D(
            filters=self.stage_channels[0],
            kernel_size=3,
            padding="same",
            use_bias=False,
        )(x)
        x = tfl.BatchNormalization(momentum=0.9)(x)
        x = tf.nn.relu(x)
        branches = [x]
        branches.append(nbranch)

        # Stage 2-4
        for i in range(1, len(self.stage_channels)):
            for _ in range(self.stage_blocks[i] - 1):
                branches = add_residual_units(branches, self.stage_channels, units=4)
                branches = add_exchange_unit(branches, self.stage_channels)
            branches = add_residual_units(branches, self.stage_channels, units=4)

            if i < len(self.stage_channels) - 1:
                branches = add_exchange_unit(branches, self.stage_channels)
                nbranch = split_new_branch(branches, self.stage_channels)
                branches.append(nbranch)
            else:
                branches = add_exchange_unit(branches, self.stage_channels)
                pass

        # Only the main branch is required as output
        x = branches[0]

        # One heatmap for each joint
        x = tfl.Conv2D(filters=self.num_joints, kernel_size=1, use_bias=True)(x)

        # Cast last layer to float32 for numeric stability with mixed precision
        output_tensor = tf.cast(x, dtype=tf.float32, name="output")

        model = tf.keras.Model(input_tensor, output_tensor, name="HRnet")
        return model

    # ==============================================================================================

    def summary(self, line_length=100, **kwargs):  # pylint: disable=arguments-differ
        print("")
        self.model.summary(line_length=line_length, **kwargs)

    # ==============================================================================================

    @tf.function()
    def call(self, x, training=False):  # pylint: disable=arguments-differ
        """Call with RGB image"""

        x = self.model(x, training=training)
        return x
