import tensorflow as tf

from . import coco
from . import pl_tools as pltools

# ==================================================================================================

AUTOTUNE = tf.data.experimental.AUTOTUNE
input_height: int
input_width: int
heatmap_height: int
heatmap_width: int

imagenet_means = [0.485, 0.456, 0.406]
imagenet_stds = [0.229, 0.224, 0.225]


# ==================================================================================================


def initialize(config):
    global input_height, input_width, heatmap_height, heatmap_width

    input_height = config["image_params"]["input_height"]
    input_width = config["image_params"]["input_width"]
    heatmap_height = config["image_params"]["heatmap_height"]
    heatmap_width = config["image_params"]["heatmap_width"]


# ==================================================================================================


def process_data(sample, config, train_mode):
    """Cut out human from image and adjust keypoints positions
    Partially from: https://github.com/wmcnally/evopose2d/blob/master/dataset/dataloader.py#L63"""

    img = tf.cast(sample["raw_image"], tf.float32)
    keypts = tf.cast(sample["keypoints"], tf.float32)

    # Apply preprocessing steps. Run before cutting out images, that paddings are filled with zeros.
    img = img / 255.0
    img = img - [[imagenet_means]]
    img = img / [[imagenet_stds]]

    # Scale box to fixed size
    w, h = sample["bbox"][2], sample["bbox"][3]
    aspect_ratio = input_width / input_height
    if w > aspect_ratio * h:
        h = w / aspect_ratio
    scale = (h * 1.25) / input_height

    # Get center of bbox
    bbox = sample["bbox"]
    x, y, w, h = bbox[0], bbox[1], bbox[2], bbox[3]
    center = [x + w / 2.0, y + h / 2.0]
    center = tf.cast(tf.stack(center), tf.float32)

    # Cutout image and get transformation matrix
    img, M = pltools.transform(img, scale, 0.0, center, [input_height, input_width])
    sample["image"] = img
    sample["transMatrix"] = M

    # Apply transformation to keypoints
    kp_shape = tf.shape(keypts)
    xy = tf.reshape(keypts[..., :2], [-1, 2])
    xy = tf.transpose(tf.matmul(M[:, :2], xy, transpose_b=True)) + M[:, -1]
    xy = tf.reshape(xy, [kp_shape[0], kp_shape[1], 2])

    # Adjust visibility if coordinates are outside crop
    vis = keypts[..., 2]
    vis *= tf.cast(
        (
            (xy[..., 0] >= 0)
            & (xy[..., 0] < input_width)
            & (xy[..., 1] >= 0)
            & (xy[..., 1] < input_height)
        ),
        tf.float32,
    )
    keypts = tf.concat([xy, tf.expand_dims(vis, axis=-1)], axis=-1)
    sample["keypoints"] = keypts

    # Filter for invisible keypoints while training
    visible = tf.cast(sample["keypoints"][..., -1] > 0, tf.float32)
    sample["visible"] = visible

    sample.pop("raw_image")
    return sample


# ==================================================================================================


def create_pipeline(
    datapath: str, batch_size: int, config: dict, mode: str, detectionpath: str = ""
):
    """Create data-iterator from tab separated csv file"""

    print("\nBuilding data pipeline ...")
    train_mode = bool(mode in ["train"])

    # Initialize pipeline values, using config from method call, that we can easily reuse the config
    # from exported checkpoints
    initialize(config)

    # Load labels
    if mode != "test":
        labels = coco.ds_top_down(datapath)
    else:
        labels = coco.ds_top_down(
            datapath, detectionpath, config["bbox_score_threshold"]
        )

    print(labels[0])

    # # Convert to dict-of-lists format and create dataset
    # # Using the "from_generator" method instead resulted in the network not learning anything,
    # # even though the output of both approaches seem to be exactly the same ...
    # labels = {k: [dic[k] for dic in labels] for k in labels[0]}
    # ds = tf.data.Dataset.from_tensor_slices(labels)

    output_types = {
        "annotids": tf.int32,
        "imageid": tf.int32,
        "raw_keypoints": tf.float32,
        "keypoints": tf.float32,
        "imgpath": tf.string,
        "raw_width": tf.int32,
        "raw_height": tf.int32,
        "bbox": tf.float32,
        "score": tf.float32,
    }
    ds = tf.data.Dataset.from_generator(lambda: iter(labels), output_types=output_types)

    # Load images
    ds = ds.map(map_func=pltools.load_image, num_parallel_calls=AUTOTUNE)

    # Cache dataset, this makes training a bit faster
    if config["use_cache"]:
        ds = ds.cache()

    # Cutout human
    pd_func = lambda x: process_data(x, config, train_mode)
    ds = ds.map(map_func=pd_func, num_parallel_calls=AUTOTUNE)

    # Make heatmap labels
    mh_func = lambda x: pltools.make_heatmaps(
        x,
        hmap_size=[heatmap_height, heatmap_width],
        scale_xy=[heatmap_width / input_width, heatmap_height / input_height],
    )
    ds = ds.map(mh_func, num_parallel_calls=AUTOTUNE)
    ds = ds.map(pltools.combine_heatmaps, num_parallel_calls=AUTOTUNE)

    # Dropping remainder solved "Gradients do not exist for variables" for batchnorm layers
    ds = ds.batch(batch_size, drop_remainder=train_mode)
    ds = ds.prefetch(1)
    return ds
