import tensorflow as tf
from tensorflow.python.keras.layers.preprocessing import (
    image_preprocessing as tf_image_ops,
)


# ==================================================================================================


def load_image(sample):
    img = tf.io.read_file(sample["imgpath"])
    img = tf.io.decode_jpeg(img, channels=3)
    img = tf.cast(img, dtype=tf.uint8)

    sample["raw_image"] = img
    return sample


# ==================================================================================================


def make_heatmaps(sample, hmap_size, scale_xy):
    """Create a multichannel label image with a gaussian distribution centered at the keypoint"""

    # Scale keypoints to heatmap size
    scale = [scale_xy[0], scale_xy[1], 1]
    keypts = tf.cast(sample["keypoints"], dtype=tf.float32)
    keypts = keypts * scale

    # Round and reshape keypoints
    kp_shape = tf.shape(keypts)
    mx = tf.floor(keypts[..., 0] + 0.5)
    mx = tf.reshape(mx, [1, 1, -1])
    my = tf.floor(keypts[..., 1] + 0.5)
    my = tf.reshape(my, [1, 1, -1])

    # Create xy-grids with heatmap shape
    x = [float(i) for i in range(hmap_size[1])]
    y = [float(i) for i in range(hmap_size[0])]
    xx, yy = tf.meshgrid(x, y)
    xx = tf.reshape(xx, (hmap_size[0], hmap_size[1], 1))
    yy = tf.reshape(yy, (hmap_size[0], hmap_size[1], 1))

    # Create heatmap
    # See: https://en.wikipedia.org/wiki/Gaussian_function#Two-dimensional_Gaussian_function
    sx = sy = int((hmap_size[0] / 64) * 2)
    g1 = ((xx - mx) ** 2) / (2.0 * sx ** 2)
    g2 = ((yy - my) ** 2) / (2.0 * sy ** 2)
    hmap = tf.exp(-(g1 + g2))

    # Invisible keypoints are still drawn in the 0-0 edge or somewhere else after transformations,
    # so set them to zero to remove them from the combined heatmap image
    visible_mask = tf.reshape(sample["visible"], [1, 1, -1])
    hmap = hmap * visible_mask

    # Combine heatmaps from multiple labels (when using bottom-up)
    if kp_shape[0] > 1:
        hmap_shape = tf.shape(hmap)
        hmap = tf.reshape(
            hmap, [hmap_shape[0], hmap_shape[1], kp_shape[0], kp_shape[1]]
        )
        hmap = tf.reduce_max(hmap, axis=2)

    # Combine visible mask as well
    visible = tf.reduce_max(sample["visible"], axis=0)
    sample["visible"] = visible

    sample["heatmap"] = hmap
    return sample


# ==================================================================================================


def combine_heatmaps(sample):
    """Combine heatmaps to a single image for visualization"""

    heatmap_combined = tf.expand_dims(
        tf.reduce_max(sample["heatmap"], axis=-1), axis=-1
    )
    sample["heatmap_combined"] = heatmap_combined
    return sample


# ==================================================================================================


def transform(img, scale, angle, bbox_center, output_shape):
    """Apply scaling and rotations to the bounding box and cut it from the image.
    From: https://github.com/wmcnally/evopose2d/blob/master/dataset/dataloader.py#L27"""

    tx = bbox_center[0] - output_shape[1] * scale / 2
    ty = bbox_center[1] - output_shape[0] * scale / 2

    # For offsetting translations caused by rotation:
    # https://docs.opencv.org/2.4/modules/imgproc/doc/geometric_transformations.html
    rx = (1 - tf.cos(angle)) * output_shape[1] * scale / 2 - tf.sin(
        angle
    ) * output_shape[0] * scale / 2
    ry = (
        tf.sin(angle) * output_shape[1] * scale / 2
        + (1 - tf.cos(angle)) * output_shape[0] * scale / 2
    )

    transform = [
        scale * tf.cos(angle),
        scale * tf.sin(angle),
        rx + tx,
        -scale * tf.sin(angle),
        scale * tf.cos(angle),
        ry + ty,
        0.0,
        0.0,
    ]

    img = tf_image_ops.transform(
        tf.expand_dims(img, axis=0),
        tf.expand_dims(transform, axis=0),
        fill_mode="constant",
        output_shape=output_shape[:2],
    )
    img = tf.squeeze(img)

    # Transform for keypoints
    alpha = 1 / scale * tf.cos(-angle)
    beta = 1 / scale * tf.sin(-angle)
    transform_xy = [[alpha, beta], [-beta, alpha]]

    rx_xy = (1 - alpha) * bbox_center[0] - beta * bbox_center[1]
    ry_xy = beta * bbox_center[0] + (1 - alpha) * bbox_center[1]

    tx_xy = bbox_center[0] - output_shape[1] / 2
    ty_xy = bbox_center[1] - output_shape[0] / 2

    M = tf.concat([transform_xy, [[rx_xy - tx_xy], [ry_xy - ty_xy]]], axis=1)
    return img, M
