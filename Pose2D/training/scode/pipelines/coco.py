"""
COCO dataset (https://cocodataset.org)

Keypoint format:
(x, y, visible)
https://cocodataset.org/#format-data
"""

import copy
import json
import os

import tqdm
from pycocotools.coco import COCO

# ==================================================================================================


keypoints = {
    0: "nose",
    1: "left_eye",
    2: "right_eye",
    3: "left_ear",
    4: "right_ear",
    5: "left_shoulder",
    6: "right_shoulder",
    7: "left_elbow",
    8: "right_elbow",
    9: "left_wrist",
    10: "right_wrist",
    11: "left_hip",
    12: "right_hip",
    13: "left_knee",
    14: "right_knee",
    15: "left_ankle",
    16: "right_ankle",
}

extra_keypoints = {
    17: "mid_hip",
    18: "mid_shoulder",
}

sample_template = {
    "annotids": [],
    "imageid": 0,
    "raw_keypoints": [],
    "keypoints": [],
    "imgpath": "",
    "raw_width": 0,
    "raw_height": 0,
    "bbox": [],
    "score": 0,
}

# ==================================================================================================


def ds_top_down(datapath: str, labelpath: str = "", bbox_score_threshold: float = 0.0):

    if labelpath != "":
        samples = load_detections(datapath, labelpath, bbox_score_threshold)
    else:
        samples = load_labels(datapath)

    print("Found {} valid labels".format(len(samples)))
    return samples


# ==================================================================================================


def ds_bottom_up(datapath: str):

    # Load labels similar to top down approach
    samples = load_labels(datapath)

    # Add some additional keypoints
    samples = add_extra_keypoints(samples)

    # Group samples that share the same image
    imgs = {}
    for sample in samples:

        if sample["imgpath"] in imgs:
            imgs[sample["imgpath"]].append(sample)
        else:
            imgs[sample["imgpath"]] = [sample]

    # Combine keypoint data of all samples from the same image
    new_samples = []
    for _, samps in imgs.items():
        new_sample = copy.deepcopy(sample_template)

        new_sample["imageid"] = samps[0]["imageid"]
        new_sample["imgpath"] = samps[0]["imgpath"]
        new_sample["raw_width"] = samps[0]["raw_width"]
        new_sample["raw_height"] = samps[0]["raw_height"]

        for sample in samps:
            new_sample["annotids"].extend(sample["annotids"])
            new_sample["raw_keypoints"].append(sample["raw_keypoints"])
            new_sample["keypoints"].append(sample["keypoints"])

        new_samples.append(new_sample)

    print("Found {} valid labels".format(len(new_samples)))
    return new_samples


# ==================================================================================================


def add_extra_keypoints(samples):
    for sample in samples:
        kpts = sample["keypoints"][0]

        mid_hip = [
            (kpts[11][0] + kpts[12][0]) / 2.0,
            (kpts[11][1] + kpts[12][1]) / 2.0,
            min(kpts[11][2], kpts[12][2]),
        ]
        mid_shoulder = [
            (kpts[5][0] + kpts[6][0]) / 2.0,
            (kpts[5][1] + kpts[6][1]) / 2.0,
            min(kpts[5][2], kpts[6][2]),
        ]

        kpts.append(mid_hip)
        kpts.append(mid_shoulder)
        sample["keypoints"] = kpts

    return samples


# ==================================================================================================


def load_labels(datapath):
    """Load boxes and keypoints from label file"""

    coco_kps = COCO(datapath)
    kps = coco_kps.getAnnIds(iscrowd=0)

    print("Loading labels ...")
    samples = []
    for kp in tqdm.tqdm(kps):
        sample = copy.deepcopy(sample_template)
        annot = coco_kps.loadAnns(kp)[0]

        sample["annotids"] = [kp]
        sample["imageid"] = annot["image_id"]

        # Ignore images without keypoints
        if annot["num_keypoints"] == 0:
            continue

        # Split list of keypoints into triples (x,y,v)
        # v=0: not labeled, v=1: labeled but not visible, and v=2: labeled and visible
        nkpts = len(annot["keypoints"])
        keypts = annot["keypoints"]
        sample["raw_keypoints"] = keypts
        keypts = [keypts[i * 3 : i * 3 + 3] for i in range(int(nkpts / 3))]
        for kp in keypts:
            if kp[2] == 0:
                kp[0:2] = [0, 0]
        sample["keypoints"] = [keypts]

        imgdata = coco_kps.loadImgs(sample["imageid"])[0]
        sample["raw_height"] = imgdata["height"]
        sample["raw_width"] = imgdata["width"]
        imgpath = imgdata["coco_url"].replace("http://images.cocodataset.org/", "")
        imgpath = os.path.join(os.path.dirname(datapath), "../images/", imgpath)
        sample["imgpath"] = imgpath

        if not os.path.exists(imgpath):
            continue

        # Make sure the box fits to the image
        bbox = annot["bbox"]
        bbox[0] = max(0, bbox[0])
        bbox[1] = max(0, bbox[1])
        bbox[2] = min(bbox[2], sample["raw_width"] - bbox[0])
        bbox[3] = min(bbox[3], sample["raw_height"] - bbox[1])
        sample["bbox"] = bbox

        if annot["area"] <= 0 or bbox[2] <= 0 or bbox[3] <= 0:
            continue

        sample["score"] = 1.0
        samples.append(sample)

    return samples


# ==================================================================================================


def load_detections(datapath, labelpath, bbox_score_threshold):
    """Load boxes from person detector predictions"""

    coco_kps = COCO(datapath)

    print("Loading person detections ...")
    with open(labelpath, "r", encoding="utf-8") as file:
        data = json.load(file)

    samples = []
    for entry in tqdm.tqdm(data):
        sample = dict(sample_template)

        score = entry["score"]
        if score < bbox_score_threshold:
            continue

        imgdata = coco_kps.loadImgs(entry["image_id"])[0]
        imgpath = imgdata["coco_url"].replace("http://images.cocodataset.org/", "")
        imgpath = os.path.join(os.path.dirname(datapath), "../images/", imgpath)

        sample["bbox"] = entry["bbox"]
        sample["score"] = entry["score"]
        sample["imgpath"] = entry["imgpath"]
        sample["keypoints"] = [[0, 0, 0] for _ in range(17)]
        sample["imageid"] = entry["imageid"]
        samples.append(sample)

    return samples
