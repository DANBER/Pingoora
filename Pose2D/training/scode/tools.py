import json
import os

import numpy as np
import tensorflow as tf
from pycocotools.cocoeval import COCOeval

# ==================================================================================================


def argmax_2d(tensor):
    """Get max pixel value coordinate per channel"""

    # Input format: BxHxWxC
    tf.debugging.assert_rank(tensor, 4)

    # Flatten tensor along width-height axis and get argmax
    flat_tensor = tf.reshape(tensor, (tf.shape(tensor)[0], -1, tf.shape(tensor)[-1]))
    argmax = tf.cast(tf.argmax(flat_tensor, axis=1), tf.int32)

    # Convert indicies into 2D coordinates
    argmax_y = argmax // tf.shape(tensor)[2]
    argmax_x = argmax % tf.shape(tensor)[2]

    tensor = tf.stack((argmax_x, argmax_y), axis=1)
    return tensor


# ==================================================================================================


def center_of_mass_2d(tensor):
    """Calculate x-y center of mass per channel"""

    # Input format: BxHxWxC
    tf.debugging.assert_rank(tensor, 4)

    # Build a flat tensor which contains indicies of the image positions
    x = tf.range(tf.shape(tensor)[2], dtype=tensor.dtype)
    y = tf.range(tf.shape(tensor)[1], dtype=tensor.dtype)
    xx, yy = tf.meshgrid(x, y)
    flat_grid = tf.stack([tf.reshape(xx, (-1,)), tf.reshape(yy, (-1,))], axis=-1)
    flat_grid = tf.expand_dims(flat_grid, axis=0)

    # Reshape tensor to [batchsize*channels, height*width]
    flat_tensor = tf.transpose(tensor, perm=[0, 3, 1, 2])
    flat_tensor = tf.reshape(
        flat_tensor, (tf.shape(tensor)[0] * tf.shape(tensor)[-1], -1, 1)
    )

    # Find center by multiplying values with indicies and averaging them with the total mass
    channel_mass = tf.reduce_sum(flat_tensor, axis=1)
    center_of_mass = tf.reduce_sum(flat_tensor * flat_grid, axis=1) / channel_mass

    # Reshape back to [batchsize, channels, 2]
    center_of_mass = tf.reshape(center_of_mass, (-1, tf.shape(tensor)[-1], 2))
    return center_of_mass


# ==================================================================================================


def signop(x):
    """Helper to calculate sign op because it is not supported by tflite"""

    ispositive = tf.cast(x > 0.0, dtype=x.dtype)
    isnegative = tf.cast(x < 0.0, dtype=x.dtype)

    sign = -1.0 * isnegative + 1.0 * ispositive
    return sign


# ==================================================================================================


def calculate_keypoints_peaked(heatmap):
    """Find the maximum and second highest x-y coordinate. Move the keypoint from the maximum
    coordinate a bit into the direction of the largest neighbours."""

    # Get scores for keypoints
    scores = tf.reduce_max(tf.reduce_max(heatmap, axis=2), axis=1)
    scores = tf.expand_dims(scores, axis=-1)

    # Get keypoints with maximum response
    keypoints = argmax_2d(heatmap)
    keypoints = tf.transpose(keypoints, [0, 2, 1])
    keypoints = tf.gather(keypoints, [1, 0], axis=2)

    # Which keypoints have all 4 neighbouring pixels?
    mask = tf.cast(
        (
            (1 < keypoints[:, :, 0])
            & (keypoints[:, :, 0] < tf.shape(heatmap)[1] - 1)
            & (1 < keypoints[:, :, 1])
            & (keypoints[:, :, 1] < tf.shape(heatmap)[2] - 1)
        ),
        tf.float32,
    )

    # Make sure there are no keypoints on a boarder side
    ky = tf.maximum(1, tf.minimum(keypoints[:, :, 0], tf.shape(heatmap)[1] - 2))
    kx = tf.maximum(1, tf.minimum(keypoints[:, :, 1], tf.shape(heatmap)[2] - 2))

    # Build indices of neighbouring pixels
    channels = tf.expand_dims(tf.range(tf.shape(heatmap)[-1]), axis=0)
    channels = tf.repeat(channels, tf.expand_dims(tf.shape(heatmap)[0], axis=0), axis=0)
    k1 = tf.stack((ky, kx + 1, channels), axis=1)
    k2 = tf.stack((ky, kx - 1, channels), axis=1)
    k3 = tf.stack((ky + 1, kx, channels), axis=1)
    k4 = tf.stack((ky - 1, kx, channels), axis=1)
    k1 = tf.transpose(k1, [0, 2, 1])
    k2 = tf.transpose(k2, [0, 2, 1])
    k3 = tf.transpose(k3, [0, 2, 1])
    k4 = tf.transpose(k4, [0, 2, 1])

    # Calculate difference between left-right and top-bottom pixels to get a move direction
    diffx = tf.gather_nd(heatmap, k1, batch_dims=1) - tf.gather_nd(
        heatmap, k2, batch_dims=1
    )
    diffy = tf.gather_nd(heatmap, k3, batch_dims=1) - tf.gather_nd(
        heatmap, k4, batch_dims=1
    )
    diff = tf.stack((signop(diffy), signop(diffx)), axis=1)
    diff = tf.transpose(diff, [0, 2, 1])

    # Move keypoints if they aren't edge pixels
    keypoints = tf.cast(keypoints, tf.float32)
    keypoints = keypoints + tf.expand_dims(mask, axis=-1) * diff * 0.25

    # Switch y-x axis and append scores
    keypoints = tf.gather(keypoints, [1, 0], axis=2)
    keypoints = tf.concat([keypoints, scores], axis=-1)
    return keypoints


# ==================================================================================================


def calculate_keypoint_centers(heatmap):
    """Find center of mass of heatmap per channel. Slightly lower performance than other option."""

    # Get scores for keypoints
    scores = tf.reduce_max(tf.reduce_max(heatmap, axis=2), axis=1)
    scores = tf.expand_dims(scores, axis=-1)
    scores = tf.maximum(0.0, tf.minimum(scores, 1.0))

    keypoints = center_of_mass_2d(heatmap)
    keypoints = tf.concat([keypoints, scores], axis=-1)

    return keypoints


# ==================================================================================================


def find_keypoints(model, images):
    """Find keypoints in normal and flipped images. Combines and rescales them to the input size."""

    # Flip images and append them to the normal images to predict them in a single batch
    # There was no performace improvement compared to two predictions and graph based optimization
    flipped_images = tf.image.flip_left_right(images)
    all_images = tf.concat([images, flipped_images], axis=0)

    # Predict heatmaps for images
    predictions = model.call(all_images, training=False)
    predictions = tf.cast(predictions, dtype=tf.float32)

    # Split up batch again
    predictions1 = predictions[: tf.shape(images)[0]]
    predictions2 = predictions[tf.shape(images)[0] :]

    # Shift flipped heatmap to compensate paddings of strided convolutions and flip left-right joints
    # See https://github.com/microsoft/human-pose-estimation.pytorch/issues/69#issuecomment-582169936
    # Or a more optimized approach at: https://arxiv.org/pdf/1911.07524.pdf
    predictions2 = tf.image.flip_left_right(predictions2)
    predictions2 = tf.concat(
        [tf.expand_dims(predictions2[:, :, 0, :], axis=2), predictions2[:, :, 0:-1, :]],
        axis=2,
    )
    flip_pairs = [0, 2, 1, 4, 3, 6, 5, 8, 7, 10, 9, 12, 11, 14, 13, 16, 15]
    predictions2 = tf.gather(predictions2, flip_pairs, axis=-1)

    # Average heatmaps
    heatmaps = tf.reduce_mean([predictions1, predictions2], axis=0)
    heatmaps = heatmaps / 255

    # Extract keypoints from both heatmaps
    keypoints = calculate_keypoints_peaked(heatmaps)
    # keypoints = calculate_keypoint_centers(heatmaps)

    # Scale keypoints back to input size
    # Rounding of keypoints while scaling up/down gives some position errors compared to the targets
    scale = tf.shape(images[0]) / tf.shape(heatmaps[0])
    scale = [scale[0], scale[1], 1]
    keypoints = keypoints * scale

    return keypoints, predictions1


# ==================================================================================================


def apply_inverted_transform(keypoints, transMatrix):
    """Use inverse transform to map keypoints back to original image"""

    mrow = tf.expand_dims([0.0, 0.0, 1.0], axis=0)
    mrow = tf.repeat(mrow, tf.expand_dims(tf.shape(keypoints)[0], axis=0), axis=0)
    mrow = tf.expand_dims(mrow, axis=1)
    transMatrix = tf.concat([transMatrix, mrow], axis=1)

    invMatrix = tf.linalg.inv(transMatrix)
    invMatrix = invMatrix[:, :2, :]

    scores = keypoints[:, :, 2]
    keypoints = keypoints[:, :, :2]

    keypoints = tf.transpose(keypoints, perm=[0, 2, 1])
    keypoints = tf.transpose(
        tf.matmul(invMatrix[:, :, :2], keypoints), perm=[0, 2, 1]
    ) + tf.expand_dims(invMatrix[:, :, 2], axis=1)

    keypoints = tf.concat([keypoints, tf.expand_dims(scores, axis=-1)], axis=-1)
    return keypoints


# ==================================================================================================


def reversed_keypoint_transform_tflite(keypoints, scales):
    """Map keypoints back to original image using tflite compatibel pipeline"""

    rscale = tf.stack([scales[:, 2], scales[:, 2], tf.ones_like(scales[:, 2])], axis=1)
    keypoints = keypoints * tf.expand_dims(rscale, axis=1)

    tscale = tf.stack([scales[:, 0], scales[:, 1], tf.zeros_like(scales[:, 1])], axis=1)
    keypoints = keypoints + tf.expand_dims(tscale, axis=1)

    return keypoints


# ==================================================================================================


def make_coco_entry(keypoints: np.array, imageid, score, kp_score_threshold):

    # Rescore person accuracy with keypoint scores
    score_mask = keypoints[:, 2] > kp_score_threshold
    if np.sum(score_mask) > 0:
        score = score * np.mean(keypoints[:, 2][score_mask])
    else:
        score = 0

    result = {
        "image_id": int(imageid),
        "category_id": 1,
        "score": float(score),
        "keypoints": keypoints.flatten().tolist(),
    }
    return result


# ==================================================================================================


def run_coco_eval(cocoGt, results, save_path):

    # Sort detections by image id
    test_results = sorted(results, key=lambda k: k["image_id"])

    # Save detections
    os.makedirs(os.path.dirname(save_path), exist_ok=True)
    with open(save_path, "w+", encoding="utf-8") as file:
        json.dump(test_results, file, indent=2)

    # Run evaluation
    cocoDt = cocoGt.loadRes(save_path)
    cocoEval = COCOeval(cocoGt, cocoDt, iouType="keypoints")
    cocoEval.evaluate()
    cocoEval.accumulate()
    cocoEval.summarize()

    ap = cocoEval.stats[0]
    ar = cocoEval.stats[5]
    return ap, ar
