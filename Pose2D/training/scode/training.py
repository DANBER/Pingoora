import json
import os
import shutil
import time

import numpy as np
import tensorflow as tf
from pycocotools.coco import COCO
from tensorflow.keras import mixed_precision
from tensorflow.keras.optimizers import schedules

# Required for loading exported models with tf=2.3, which is required for tflite exports
from tensorflow.python.framework.errors_impl import (
    NotFoundError,
)  # pylint: disable=no-name-in-module

from . import nets, tools, utils


# ==================================================================================================

# tf.config.run_functions_eagerly(True)
# tf.config.optimizer.set_jit(True)

config = utils.get_config()
checkpoint_dir = config["checkpoint_dir"]

if config["detection_mode"] == "top-down":
    from .pipelines import pl_top_down as pipeline
elif config["detection_mode"] == "bottom-up":
    from .pipelines import pl_bottom_up as pipeline
else:
    raise ValueError("This detection mode doesn't exist")

model: tf.keras.Model
summary_writer: tf.summary.SummaryWriter
save_manager: tf.train.CheckpointManager
optimizer: tf.keras.optimizers.Adam
strategy: tf.distribute.Strategy

global_train_batch_size: int
global_eval_batch_size: int
cocoGt: COCO

# ==================================================================================================


def get_loss(predictions, samples):
    """Calculate MSE loss"""

    # Scaling the labels to higher values helped making the network learn something in early tests
    # The other tensorflow reference implementations scaled the heatmap as well
    labels = samples["heatmap"] * 255

    # Calculate loss only for visible joints
    visible = samples["visible"]
    visible_mask = tf.expand_dims(tf.expand_dims(visible, axis=1), axis=1)

    loss = tf.reduce_mean(tf.square(predictions - labels) * visible_mask)
    model_loss = tf.cast(sum(model.losses), dtype=loss.dtype)

    return loss, model_loss


# ==================================================================================================


def train_step(samples):
    """Run a single forward and backward step and return the loss"""

    features = samples["image"]

    with tf.GradientTape() as tape:
        predictions = model(features, training=True)
        predictions = tf.cast(predictions, dtype=tf.float32)
        loss, model_loss = get_loss(predictions, samples)

        # Divide loss through number of replicas. This is important because later after the gradients
        # were calculated on each replica, they are aggregated across the replicas by summing them.
        loss = loss / strategy.num_replicas_in_sync
        model_loss = model_loss / strategy.num_replicas_in_sync

        if config["mixed_precision"]:
            scaled_loss = optimizer.get_scaled_loss(loss)
            scaled_loss += model_loss
        else:
            loss = loss + model_loss

    trainable_variables = model.trainable_variables
    if config["mixed_precision"]:
        gradients = tape.gradient(scaled_loss, trainable_variables)
        gradients = optimizer.get_unscaled_gradients(gradients)
    else:
        gradients = tape.gradient(loss, trainable_variables)

    optimizer.apply_gradients(zip(gradients, trainable_variables))

    return loss


# ==================================================================================================


def eval_step(samples):
    """Run a single forward step and return the loss"""

    if config["detection_mode"] == "top-down":
        keypoints, predictions = tools.find_keypoints(model, samples["image"])
        keypoints = tools.apply_inverted_transform(keypoints, samples["transMatrix"])
    else:
        predictions = model(samples["image"], training=False)
        predictions = tf.cast(predictions, dtype=tf.float32)
        keypoints = tf.zeros([0, 3])

    loss, _ = get_loss(predictions, samples)
    loss = loss / strategy.num_replicas_in_sync

    return loss, [keypoints, samples["imageid"], samples["score"]]


# ==================================================================================================


@tf.function()
def distributed_train_step(dist_inputs):
    """Helper function for distributed training"""

    per_replica_losses = strategy.run(train_step, args=(dist_inputs,))
    loss = strategy.reduce(tf.distribute.ReduceOp.SUM, per_replica_losses, axis=None)
    return loss


# ==================================================================================================


@tf.function()
def distributed_eval_step(dist_inputs):
    """Helper function for distributed evaluation"""

    per_replica_losses, replica_keypointdata = strategy.run(
        eval_step, args=(dist_inputs,)
    )
    loss = strategy.reduce(tf.distribute.ReduceOp.SUM, per_replica_losses, axis=None)

    if strategy.num_replicas_in_sync > 1:
        keypointdata = [tf.concat(rk.values, axis=0) for rk in replica_keypointdata]
    else:
        keypointdata = replica_keypointdata

    return loss, keypointdata


# ==================================================================================================


def log_prediction(samples, step, prefix, trainmode=True):
    """Run a prediction and log the predicted heatmap as well as input and label"""

    features = tf.expand_dims(samples["image"][0], axis=0)
    hcomb = tf.expand_dims(samples["heatmap_combined"][0], axis=0)

    if trainmode:
        # Using model.predict() instead didn't work here
        prediction = model(features, training=False)
        prediction = prediction.numpy()
    else:
        prediction = model.predict(features)

    prediction = prediction / 255
    pcomb = np.expand_dims(np.sum(prediction[0], axis=-1), axis=-1)
    pcomb = np.expand_dims(pcomb, axis=0)
    pcomb = pcomb / np.max(pcomb)

    hcomb = tf.cast(hcomb, dtype=tf.float32)
    images = features * [[pipeline.imagenet_stds]] + [[pipeline.imagenet_means]]

    with summary_writer.as_default():
        tf.summary.image(prefix + "features", features, step=step, max_outputs=1)
        tf.summary.image(prefix + "image_restored", images, step=step, max_outputs=1)
        tf.summary.image(prefix + "heatmap_combined", hcomb, step=step, max_outputs=1)
        tf.summary.image(
            prefix + "prediction_combined", pcomb, step=step, max_outputs=1
        )

        hnose = samples["heatmap"][0][..., 0]
        pnose = prediction[..., 0]
        hnose = tf.expand_dims(tf.expand_dims(hnose, axis=0), axis=-1)
        pnose = tf.expand_dims(pnose, axis=-1)
        tf.summary.image(prefix + "heatmap_nose", hnose, step=step, max_outputs=1)
        tf.summary.image(prefix + "prediction_nose", pnose, step=step, max_outputs=1)
        hhandr = samples["heatmap"][0][..., 10]
        phandr = prediction[..., 10]
        hhandr = tf.expand_dims(tf.expand_dims(hhandr, axis=0), axis=-1)
        phandr = tf.expand_dims(phandr, axis=-1)
        tf.summary.image(prefix + "heatmap_hand_r", hhandr, step=step, max_outputs=1)
        tf.summary.image(prefix + "prediction_hand_r", phandr, step=step, max_outputs=1)


# ==================================================================================================


def distributed_log_prediction(dist_inputs, step, prefix):
    """Helper function for distributed prediction logs. Because the dataset is distributed, we have
    to extract the data values of the first device before running a non distributed prediction"""

    samples = strategy.experimental_local_results(dist_inputs)[0]

    if hasattr(samples["image"], "values"):
        # Multi-GPU distribution
        conv_samps = {
            "image": samples["image"].values[0],
            "heatmap": samples["heatmap"].values[0],
            "heatmap_combined": samples["heatmap_combined"].values[0],
        }
    else:
        # Single-GPU distribution
        conv_samps = {
            "image": samples["image"],
            "heatmap": samples["heatmap"],
            "heatmap_combined": samples["heatmap_combined"],
        }

    log_prediction(conv_samps, step, prefix)


# ==================================================================================================


def train(dataset_train, dataset_eval, start_epoch, stop_epoch):
    step = 0
    best_eval_loss = float("inf")
    epochs_without_improvement = 0
    last_save_time = time.time()
    training_start_time = time.time()
    training_epochs = 0

    if config["eval_before_start"]:
        eval_loss = evaluate(dataset_eval, epoch=0)
        best_eval_loss = eval_loss

    for epoch in range(start_epoch, stop_epoch):
        start_time = time.time()
        epoch_steps = 0
        print("\nStarting new training epoch ...")

        dist_dataset_iterator = iter(dataset_train)
        for samples in dist_dataset_iterator:

            if epoch_steps in config["profile_steps"]:
                # Train step with profiling
                with tf.profiler.experimental.Profile(checkpoint_dir):
                    with tf.profiler.experimental.Trace("train", step_num=step, _r=1):
                        print("Profiling performance of next step ...")
                        samples = next(dist_dataset_iterator)
                        loss = distributed_train_step(samples)
            else:
                # Normal train step
                loss = distributed_train_step(samples)

            step += 1
            epoch_steps += 1
            print("Step: {} - Epoch: {} - Loss: {}".format(step, epoch, loss.numpy()))

            with summary_writer.as_default():
                tf.summary.experimental.set_step(step)
                tf.summary.scalar("Train Loss", loss)
                if isinstance(
                    optimizer.lr, tf.keras.optimizers.schedules.LearningRateSchedule
                ):
                    current_lr = optimizer.lr(optimizer.iterations)
                else:
                    current_lr = optimizer.lr
                tf.summary.scalar("Learning Rate", current_lr)

            if (
                step != 0
                and config["log_tensorboard_steps"] != 0
                and step % config["log_tensorboard_steps"] == 0
            ):
                distributed_log_prediction(samples, step, prefix="train_")

            if (time.time() - last_save_time) / 60 > config["autosave_every_min"]:
                save_manager.save()
                last_save_time = time.time()

        # Evaluate
        eval_loss = evaluate(dataset_eval, epoch)

        # Count epochs without improvement for early stopping and reducing learning rate on plateaus
        if eval_loss > best_eval_loss - config["esrp_min_delta"]:
            epochs_without_improvement += 1
        else:
            epochs_without_improvement = 0

        # Save new best model
        if eval_loss < best_eval_loss:
            best_eval_loss = eval_loss
            model.save_weights(checkpoint_dir)
            print("Saved new best validating model")

        training_epochs += 1
        msg = "Epoch {} took {} hours\n"
        duration = utils.seconds_to_hours(time.time() - start_time)
        print(msg.format(epoch, duration))

        # Early stopping
        es_epochs = config["early_stopping_epochs"]
        if (
            config["use_early_stopping"]
            and epochs_without_improvement == es_epochs
            and training_epochs >= es_epochs + config["es_offset_epochs"]
        ):
            msg = "Early stop triggered as the loss did not improve the last {} epochs"
            print(msg.format(epochs_without_improvement))
            break

        # Reduce learning rate on plateau. If the learning rate was reduced and there is still
        # no improvement, wait reduce_lr_plateau_epochs before the learning rate is reduced again
        rp_epochs = config["reduce_lr_plateau_epochs"]
        if (
            config["use_lrp_reduction"]
            and "schedule" not in config["optimizer"]
            and epochs_without_improvement > 0
            and training_epochs >= rp_epochs + config["es_offset_epochs"]
            and epochs_without_improvement % rp_epochs == 0
        ):
            # Reduce learning rate
            new_lr = optimizer.learning_rate * config["lr_plateau_reduction"]
            optimizer.learning_rate = new_lr
            msg = "Encountered a plateau, reducing learning rate to {}"
            print(msg.format(optimizer.learning_rate))

            # Reload checkpoint that we use the best_dev weights again
            print("Reloading model with best weights ...")
            model.load_weights(checkpoint_dir)

    msg = "\nCompleted training after {} epochs with best evaluation loss of {:.4f} after {} hours"
    duration = utils.seconds_to_hours(time.time() - training_start_time)
    print(msg.format(training_epochs, best_eval_loss, duration))


# ==================================================================================================


def evaluate(dataset_eval, epoch=0):
    print("\nEvaluating ...")
    loss = 0
    step = 0
    test_results = []
    time_start = time.time()

    for samples in dataset_eval:
        sloss, keypointdata = distributed_eval_step(samples)
        loss += sloss.numpy()

        if config["detection_mode"] == "top-down":
            keypointdata = [kd.numpy() for kd in keypointdata]
            for i in range(len(keypointdata[1])):
                kp = keypointdata[0][i]
                imgid = keypointdata[1][i]
                score = keypointdata[2][i]

                result = tools.make_coco_entry(
                    kp, imgid, score, config["keypoint_score_threshold"]
                )
                test_results.append(result)

        step += 1
        print("#", end="", flush=True)

        if (
            step != 0
            and config["log_tensorboard_steps"] != 0
            and step % int(config["log_tensorboard_steps"] / 2) == 0
        ):
            distributed_log_prediction(samples, epoch * 100 + step, prefix="eval_")

    duration = utils.seconds_to_hours(time.time() - time_start)
    print("\nEvalutation took {} hours".format(duration))

    loss = loss / step
    print("Validation loss: {}".format(loss))

    with summary_writer.as_default():
        tf.summary.scalar("Eval Loss", loss, step=epoch)

    if config["detection_mode"] == "top-down":
        file_path = os.path.join(checkpoint_dir, "results/train/predictions_{}.json")
        file_out = file_path.format(int(time.time()))
        ap, ar = tools.run_coco_eval(cocoGt, test_results, file_out)

        with summary_writer.as_default():
            tf.summary.scalar("Eval AP", ap, step=epoch)
            tf.summary.scalar("Eval AR", ar, step=epoch)

    return loss


# ==================================================================================================


def build_pipelines():
    """Initialize train/eval data pipelines"""
    global global_train_batch_size, global_eval_batch_size

    global_train_batch_size = (
        config["batch_sizes"]["train"] * strategy.num_replicas_in_sync
    )
    global_eval_batch_size = (
        config["batch_sizes"]["eval"] * strategy.num_replicas_in_sync
    )

    # Create pipelines
    dataset_train = pipeline.create_pipeline(
        datapath=config["data"]["train"],
        batch_size=global_train_batch_size,
        config=config,
        mode="train",
    )
    dataset_eval = pipeline.create_pipeline(
        datapath=config["data"]["eval"],
        batch_size=global_eval_batch_size,
        config=config,
        mode="eval",
    )

    # Getting steps per epoch only works before distributing the dataset
    steps_per_epoch = tf.data.experimental.cardinality(dataset_train).numpy()

    # Solve "Found an unshardable source dataset" warning, run before distribution
    options = tf.data.Options()
    options.experimental_distribute.auto_shard_policy = (
        tf.data.experimental.AutoShardPolicy.DATA
    )
    dataset_train = dataset_train.with_options(options)
    dataset_eval = dataset_eval.with_options(options)

    # Distribute datasets
    dataset_train = strategy.experimental_distribute_dataset(dataset_train)
    dataset_eval = strategy.experimental_distribute_dataset(dataset_eval)

    return dataset_train, dataset_eval, steps_per_epoch


# ==================================================================================================


def create_optimizer(steps_per_epoch: int):
    """Initialize training optimizer"""

    print("steps_per_epoch:", steps_per_epoch)
    optconf = config["optimizer"]

    # Scale learning rate with larger batchsizes to improve accuracy
    learning_rate = optconf["learning_rate"] * global_train_batch_size / 32
    lr_schedule = learning_rate

    if "schedule" in optconf:
        if steps_per_epoch <=0:
            # When the dataset is built with the "from_generator" approach, 
            # like in the bottom-up pipeline, it doesn't know how many samples it has
            raise ValueError("Invalid steps per epoch")

        if optconf["schedule"] == "cosine_decay":
            lr_schedule = schedules.CosineDecay(
                initial_learning_rate=learning_rate,
                decay_steps=config["training_epochs"] * steps_per_epoch,
            )
        elif optconf["schedule"] == "cosine_decay_restarts":
            lr_schedule = schedules.CosineDecayRestarts(
                initial_learning_rate=learning_rate,
                first_decay_steps=optconf["decay_epochs"] * steps_per_epoch,
                m_mul=0.75,
                t_mul=1.5,
            )
        else:
            raise ValueError

    optimizer_type = optconf["name"]
    if optimizer_type == "adam":
        optim = tf.optimizers.Adam(
            learning_rate=lr_schedule,
        )

    if config["mixed_precision"]:
        optim = mixed_precision.LossScaleOptimizer(optim)

    return optim


# ==================================================================================================


def build_new_model(
    new_config: dict, using_config_export: bool = False, print_log: bool = True
):
    # Get the model type either from the given config file (else-case)
    # or the from the model the training is continued on (if-case)
    if not using_config_export and (
        new_config["continue_pretrained"] or not new_config["empty_ckpt_dir"]
    ):
        path = os.path.join(checkpoint_dir, "config_export.json")
        exported_config = utils.load_json_file(path)
        network_type = exported_config["network"]["name"]
    else:
        network_type = new_config["network"]["name"]

    if print_log:
        print("\nCreating new {} model ...".format(network_type))

    # Create the network
    mynet = getattr(nets, network_type)
    new_model = mynet.MyModel(
        netconfig=new_config["network"],
        image_params=new_config["image_params"],
    )

    new_model.compile()
    return new_model


# ==================================================================================================


def copy_weights(exported_model, new_model) -> None:
    """Copy model weights. Compared to loading the model directly, this has the benefit that parts
    of the model code can be changed as long the layers are kept."""

    new_model.set_weights(exported_model.get_weights())


# ==================================================================================================


def load_exported_model(exported_dir: str):
    """Rebuild model and load weights, because exporting the full model and loading it again didn't
    work for some models due to problems with the input-signature for @tf.function decorator"""

    path = os.path.join(exported_dir, "config_export.json")
    if os.path.exists(path):
        exported_config = utils.load_json_file(path)
    else:
        exported_config = {}

    # If there exists a .h5 model, for example the shared models from evopose, load it and rename it
    # afterwards that on the next try the weights saved while training are loaded instead
    if any([f.endswith(".h5") for f in os.listdir(exported_dir)]):
        print("Trying to load weights from h5 model ...")

        exported_model = build_new_model(
            exported_config, using_config_export=True, print_log=False
        )
        ishape = [
            None,
            exported_config["image_params"]["input_height"],
            exported_config["image_params"]["input_width"],
            3,
        ]
        exported_model.build(input_shape=ishape)

        h5path = [f for f in os.listdir(exported_dir) if f.endswith(".h5")][0]
        h5path = os.path.join(exported_dir, h5path)
        exported_model.model.load_weights(h5path)

        os.rename(h5path, h5path.replace(".h5", ".h5old"))
        return exported_model

    # Load model from weight files only or from complete models
    try:
        print("Trying to load weights directly ...")
        path = os.path.join(exported_dir, "config_export.json")
        exported_config = utils.load_json_file(path)
        exported_model = build_new_model(
            exported_config, using_config_export=True, print_log=False
        )
        exported_model.load_weights(exported_dir)
    except (OSError, NotFoundError):
        # Load old or exported models where not only the weights were saved
        print("Loading weights from exported model instead ...")
        exported_model = tf.keras.models.load_model(exported_dir)

    return exported_model


# ==================================================================================================


def main():
    global model, summary_writer, save_manager, optimizer, strategy, cocoGt

    print("Starting training with config:")
    print(json.dumps(config, indent=2))

    # Use growing gpu memory
    gpus = tf.config.experimental.list_physical_devices("GPU")
    for gpu in gpus:
        tf.config.experimental.set_memory_growth(gpu, True)

    if config["mixed_precision"]:
        # Enable mixed precision training
        mixed_precision.set_global_policy("mixed_float16")
        print("Training with mixed precision ...")

    if config["empty_ckpt_dir"]:
        # Delete checkpoint dir
        if os.path.exists(checkpoint_dir):
            utils.delete_dir(checkpoint_dir)

    if config["continue_pretrained"]:
        # Copy the pretrained checkpoint
        shutil.copytree(config["pretrained_checkpoint_dir"], config["checkpoint_dir"])
        print("Copied pretrained checkpoint")
    else:
        # Create and empty directory
        os.makedirs(checkpoint_dir, exist_ok=True)

    # Enable training with multiple gpus
    strategy = tf.distribute.MirroredStrategy()

    # Load COCO labels
    cocoGt = COCO(config["data"]["eval_annots"])

    # Initialize data pipelines
    dataset_train, dataset_eval, steps_per_epoch = build_pipelines()

    # Create and initialize the model
    with strategy.scope():
        model = build_new_model(config)

    # Optionally load exported weights
    if config["continue_pretrained"] or not config["empty_ckpt_dir"]:
        print("Copying model weights from existing checkpoint ...")
        exported_model = load_exported_model(checkpoint_dir)
        copy_weights(exported_model, model)

    # Select optimizer
    with strategy.scope():
        optimizer = create_optimizer(steps_per_epoch)

    # Export current config next to the checkpoints
    path = os.path.join(checkpoint_dir, "config_export.json")
    with open(path, "w+", encoding="utf-8") as file:
        json.dump(config, file, indent=2)

    # Initialize checkpoint manager for intermediate model backups
    summary_writer = tf.summary.create_file_writer(checkpoint_dir)
    checkpoint = tf.train.Checkpoint(model=model, optimizer=optimizer)
    save_manager = tf.train.CheckpointManager(
        checkpoint, directory=checkpoint_dir, max_to_keep=1
    )

    # Optionally overwrite model with backup checkpoint
    if config["restore_ckpt_insteadof_pb_file"]:
        print("Overwriting model with backup from the ckpt file ...")
        with strategy.scope():
            checkpoint.restore(save_manager.latest_checkpoint)

    # Print model summary
    model.summary()
    img_path = os.path.join(config["checkpoint_dir"], "model.png")
    tf.keras.utils.plot_model(
        model.model, to_file=img_path, show_shapes=True, expand_nested=True
    )
    input_shape = (
        config["image_params"]["input_height"],
        config["image_params"]["input_width"],
        3,
    )
    flops = utils.get_flops(model.model, input_shape) / 1e9 / 2
    print("The model has {:.2f}G multiply-add operations".format(flops))

    # Optionally save model before doing any training updates
    if config["save_fresh_model"]:
        model.save_weights(checkpoint_dir)
        print("Saved fresh model")

    # Finally the training can start
    start_epoch = 1
    max_epoch = config["training_epochs"]
    train(dataset_train, dataset_eval, start_epoch, max_epoch)
