#! /bin/bash

xhost +
docker run --privileged --rm --network host -it \
  --gpus all --shm-size=16g --ulimit memlock=-1 --ulimit stack=67108864 \
  --volume "$(pwd)"/Pingoora/Pose2D/:/Pingoora/Pose2D/ \
  --volume "$(pwd)"/checkpoints/:/checkpoints/ \
  --volume "$(pwd)"/datasets/:/datasets/ \
  --volume /tmp/.X11-unix:/tmp/.X11-unix \
  --env DISPLAY --env QT_X11_NO_MITSHM=1 \
  pingoora_pose2d
