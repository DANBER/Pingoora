# Pingoora - Pose2D

Scripts and models for 2D pose recognition.

<br>

## Usage

#### General infos

Build and run docker container:

```bash
docker build -f Pingoora/Pose2D/Containerfile -t pingoora_pose2d ./Pingoora/Pose2D/

./Pingoora/Pose2D/run_container.sh
```


Get COCO dataset: \
(links are at the top of the page https://cocodataset.org/#download) \
(will take up 27GB of space + same amount for temporary download files)

```bash
mkdir -p datasets/coco2017/ && cd datasets/coco2017/
wget http://images.cocodataset.org/zips/val2017.zip
wget http://images.cocodataset.org/annotations/annotations_trainval2017.zip
mkdir -p images/ && mkdir -p annotations/
unzip val2017.zip -d images/
unzip annotations_trainval2017.zip
rm *.zip
```

Test COCO demo scripts:
```bash
python3 /Pingoora/Pose2D/extras/coco_demo.py
```

#### Training and evaluation

Use the template and edit `training/config/train_config.yaml` to your needs.

Run scripts:
```bash
python3 /Pingoora/Pose2D/training/run_tests.py
python3 /Pingoora/Pose2D/training/run_train.py
```

Tensorboard logs:
```bash
tensorboard --logdir /checkpoints/
```

## Performace results

ResNet-50 - 256x192 - Best evaluation loss: 44.9786
```bash
# Test on validation groundtruth boxes
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=   all | maxDets= 20 ] = 0.715
 Average Precision  (AP) @[ IoU=0.50      | area=   all | maxDets= 20 ] = 0.915
 Average Precision  (AP) @[ IoU=0.75      | area=   all | maxDets= 20 ] = 0.789
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=medium | maxDets= 20 ] = 0.685
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= large | maxDets= 20 ] = 0.759
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets= 20 ] = 0.747
 Average Recall     (AR) @[ IoU=0.50      | area=   all | maxDets= 20 ] = 0.930
 Average Recall     (AR) @[ IoU=0.75      | area=   all | maxDets= 20 ] = 0.812
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=medium | maxDets= 20 ] = 0.713
 Average Recall     (AR) @[ IoU=0.50:0.95 | area= large | maxDets= 20 ] = 0.798
AP: 0.7146085427154996 --- AR: 0.7470403022670025

# Test on validation detections
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=   all | maxDets= 20 ] = 0.695
 Average Precision  (AP) @[ IoU=0.50      | area=   all | maxDets= 20 ] = 0.884
 Average Precision  (AP) @[ IoU=0.75      | area=   all | maxDets= 20 ] = 0.770
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=medium | maxDets= 20 ] = 0.661
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= large | maxDets= 20 ] = 0.763
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets= 20 ] = 0.770
 Average Recall     (AR) @[ IoU=0.50      | area=   all | maxDets= 20 ] = 0.929
 Average Recall     (AR) @[ IoU=0.75      | area=   all | maxDets= 20 ] = 0.832
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=medium | maxDets= 20 ] = 0.721
 Average Recall     (AR) @[ IoU=0.50:0.95 | area= large | maxDets= 20 ] = 0.837
AP: 0.695346546361697 --- AR: 0.7697890428211588
```

HRNet-W32 - 256x192 - Best evaluation loss: 40.5075 - Training duration (133 epochs): 17:07:38
```bash
# Test on validation groundtruth boxes
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=   all | maxDets= 20 ] = 0.743
 Average Precision  (AP) @[ IoU=0.50      | area=   all | maxDets= 20 ] = 0.925
 Average Precision  (AP) @[ IoU=0.75      | area=   all | maxDets= 20 ] = 0.813
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=medium | maxDets= 20 ] = 0.716
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= large | maxDets= 20 ] = 0.786
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets= 20 ] = 0.774
 Average Recall     (AR) @[ IoU=0.50      | area=   all | maxDets= 20 ] = 0.936
 Average Recall     (AR) @[ IoU=0.75      | area=   all | maxDets= 20 ] = 0.838
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=medium | maxDets= 20 ] = 0.743
 Average Recall     (AR) @[ IoU=0.50:0.95 | area= large | maxDets= 20 ] = 0.822
AP: 0.7426358086673674 --- AR: 0.7744175062972293

# Test on validation detections
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=   all | maxDets= 20 ] = 0.722
 Average Precision  (AP) @[ IoU=0.50      | area=   all | maxDets= 20 ] = 0.888
 Average Precision  (AP) @[ IoU=0.75      | area=   all | maxDets= 20 ] = 0.790
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=medium | maxDets= 20 ] = 0.689
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= large | maxDets= 20 ] = 0.787
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets= 20 ] = 0.791
 Average Recall     (AR) @[ IoU=0.50      | area=   all | maxDets= 20 ] = 0.933
 Average Recall     (AR) @[ IoU=0.75      | area=   all | maxDets= 20 ] = 0.848
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=medium | maxDets= 20 ] = 0.745
 Average Recall     (AR) @[ IoU=0.50:0.95 | area= large | maxDets= 20 ] = 0.855
AP: 0.7217029269775422 --- AR: 0.7912783375314862
```

<br>

Epoch times (2xNvidia-1080Ti + Intel-i7-8700K)
- PyTorch (v1.8.0, Nvidia container 20.12 (the 21.08 container threw some strange system-lib error), HRNet Repo)
  - ResNet50-192x256: 14.7/14.9 min for 1st/2nd epoch, BatchSize=32
  - HRnetW32-192x256: 33.1/31.0 min, BS=16
- This TF2 implementation (v2.5.0, Nvidia container 21.08)
  - ResNet50-192x256: 14.1/13.6 min with BS=32 and 11.5/11.2 min with BS=64
  - HRnetW32-192x256: 30.8/29.9 min with BS=16

<br>

## Acknowledgements

- EvoPose: https://github.com/wmcnally/evopose2d
- TFSimple: https://github.com/mks0601/TF-SimpleHumanPose
- HRNet: https://github.com/HRNet/HRNet-Human-Pose-Estimation
- SimpleBaselines: https://github.com/Microsoft/human-pose-estimation.pytorch

TODO:
 - Bottom up pipeline fix
