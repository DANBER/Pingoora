"""Taken from: https://github.com/cocodataset/cocoapi/blob/master/PythonAPI/pycocoDemo.ipynb"""

import matplotlib.pyplot as plt
import numpy as np
import pylab
import skimage.io as io
from pycocotools.coco import COCO

# ==================================================================================================

pylab.rcParams["figure.figsize"] = (8.0, 10.0)
dataDir = "/datasets/coco2017/"
dataType = "val2017"
example_tags = ["person", "dog"]

# ==================================================================================================

# initialize COCO api for instance annotations
annFile = "{}/annotations/instances_{}.json".format(dataDir, dataType)
coco = COCO(annFile)

# display COCO categories and supercategories
cats = coco.loadCats(coco.getCatIds())
nms = [cat["name"] for cat in cats]
print("COCO categories: \n{}\n".format(" ".join(nms)))

nms = set([cat["supercategory"] for cat in cats])
print("COCO supercategories: \n{}".format(" ".join(nms)))

# get all images containing given categories, select one at random
catIds = coco.getCatIds(catNms=example_tags)
imgIds = coco.getImgIds(catIds=catIds)
print("Found {} images with the example tags".format(len(imgIds)))
img = coco.loadImgs(imgIds[np.random.randint(0, len(imgIds))])[0]
print(img)

# load and display image
# I = io.imread('%s/images/%s/%s'%(dataDir,dataType,img['file_name']))
# use url to load image
I = io.imread(img["coco_url"])
plt.axis("off")
plt.imshow(I)
plt.show()

# load and display instance annotations
plt.imshow(I)
plt.axis("off")
annIds = coco.getAnnIds(imgIds=img["id"], catIds=catIds, iscrowd=None)
anns = coco.loadAnns(annIds)
coco.showAnns(anns)

# initialize COCO api for person keypoints annotations
annFile = "{}/annotations/person_keypoints_{}.json".format(dataDir, dataType)
coco_kps = COCO(annFile)
print("A:", coco_kps.getAnnIds(iscrowd=None)[0])
print("B:", coco_kps.loadAnns(coco_kps.getAnnIds(iscrowd=None)[0]))

# load and display keypoints annotations
plt.imshow(I)
plt.axis("off")
ax = plt.gca()
annIds = coco_kps.getAnnIds(imgIds=img["id"], catIds=catIds, iscrowd=None)
anns = coco_kps.loadAnns(annIds)
coco_kps.showAnns(anns)

# initialize COCO api for caption annotations
annFile = "{}/annotations/captions_{}.json".format(dataDir, dataType)
coco_caps = COCO(annFile)

# load and display caption annotations
annIds = coco_caps.getAnnIds(imgIds=img["id"])
anns = coco_caps.loadAnns(annIds)
coco_caps.showAnns(anns)
plt.imshow(I)
plt.axis("off")
plt.show()
