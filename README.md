# Pingoora

Pose Recognition in 2D and 3D.

<br>

#### General infos

File structure will look as follows:

```text
my_posetracking_folder
    checkpoints
    datasets
    Pingoora            <- This repository
```
